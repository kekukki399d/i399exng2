import { Routes } from '@angular/router';

import { ListComponent } from './list/list.cmp';
import { ViewComponent } from './view/view.cmp';

export const routes: Routes = [
    { path: 'list', component: ListComponent },
    { path: 'view/:id', component: ViewComponent },
    { path: '', redirectTo: 'list', pathMatch: 'full' }
];