import { Component, OnInit } from '@angular/core';
import { Task, TaskService } from '../task.srv';

@Component({
    selector: 'list',
    templateUrl: 'app/list/list.html',
    styleUrls: ['app/list/list.css']
})
export class ListComponent implements OnInit {

    tasks: Task[] = [];
    newTaskTitle: string;

    constructor(private taskService: TaskService) {}

    private updateTasks(): void {
        this.taskService.getTasks().then(tasks => this.tasks = tasks);
    }

    addNewTask(): void {
        this.taskService.saveTask(new Task(this.newTaskTitle))
            .then(() => {
                this.updateTasks();
                this.newTaskTitle = '';
            });
    }

    deleteTask(taskId : number): void {
        this.taskService.deleteTask(taskId)
            .then(() => this.updateTasks());
    }

    ngOnInit(): void {
        this.updateTasks();
    }

}
