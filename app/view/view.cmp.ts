import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

import { TaskService } from '../task.srv';

@Component({
    templateUrl: 'app/view/view.html'
})
export class ViewComponent implements OnInit {

    constructor(private route: ActivatedRoute,
                private router: Router,
                private taskService: TaskService) {}

    back() {
    }

    ngOnInit(): void {
    }

}
