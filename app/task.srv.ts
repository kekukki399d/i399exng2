import { Http, Response } from "@angular/http";
import { Injectable } from "@angular/core";

export class Task {
    added: Date;
    _id: number;
    done: boolean = false;

    constructor(public title : string) {
        this.added = new Date();
    };
}

@Injectable()
export class TaskService {
    constructor(private http: Http) {}

    getTasks(): Promise<Task[]> {
        return this.http
            .get('api/tasks')
            .toPromise()
            .then((response: Response) => response.json());
    }

    getTask(id: number): Promise<Task> {
        return this.http
            .get('api/tasks/' + id)
            .toPromise()
            .then((response: Response) => response.json());
    }

    saveTask(task: Task): Promise<void> {
        return this.http
            .post('api/tasks', task)
            .toPromise()
            .then(() => <void>null);
    }

    deleteTask(id: number): Promise<void> {
        return this.http
            .delete('api/tasks/' + id)
            .toPromise()
            .then(() => <void>null);
    }

}